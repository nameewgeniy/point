<?php namespace Ghost\Point\Interfaces;


use Ghost\Point\Dto\OrderDto;

interface OrderServiceInterface
{
    public function create(OrderDto $orderDto);
}