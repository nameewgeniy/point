<?php namespace Ghost\Point\Dto;


use DateTime;

class OrderDto
{
    /** @var $name string */
    private $name;
    /** @var $phone string */
    private $phone;
    /** @var $email string */
    private $email;
    /** @var $address string */
    private $address;
    /** @var $date string */
    private $date;
    /** @var $payment_method string*/
    private $payment_method;
    /** @var $items OrderItemDto[] */
    private $items;

    /**
     * @return string
     */
    public function getPaymentMethod(): string
    {
        return $this->payment_method;
    }

    /**
     * @param string $payment_method
     */
    public function setPaymentMethod(string $payment_method)
    {
        $this->payment_method = $payment_method;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return OrderItemDto[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param OrderItemDto[] $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * @param $item
     */
    public function addItem(OrderItemDto $item)
    {
        $this->items[] = $item;
    }

}