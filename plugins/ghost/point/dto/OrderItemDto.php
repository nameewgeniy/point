<?php namespace Ghost\Point\Dto;


class OrderItemDto
{
    private $product_id;
    private $device_id;
    private $volume;
    private $device_number;
    private $device_brand;

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     * @param mixed $device_id
     */
    public function setDeviceId($device_id)
    {
        $this->device_id = $device_id;
    }

    /**
     * @return mixed
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param mixed $volume
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    /**
     * @return mixed
     */
    public function getDeviceNumber()
    {
        return $this->device_number;
    }

    /**
     * @param mixed $device_number
     */
    public function setDeviceNumber($device_number)
    {
        $this->device_number = $device_number;
    }

    /**
     * @return mixed
     */
    public function getDeviceBrand()
    {
        return $this->device_brand;
    }

    /**
     * @param mixed $device_brand
     */
    public function setDeviceBrand($device_brand)
    {
        $this->device_brand = $device_brand;
    }


}