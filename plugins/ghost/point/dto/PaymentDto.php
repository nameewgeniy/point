<?php


namespace Ghost\Point\Dto;


class PaymentDto
{
    private $description;
    private $amount;
    private $metadata;
    private $payment_method_data;
    private $confirmation;
}