<?php namespace Ghost\Point\Models;

use Model;

/**
 * Model
 */
class Device extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ghost_point_devices';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
