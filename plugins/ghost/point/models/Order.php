<?php namespace Ghost\Point\Models;

use Cms\Classes\Theme;
use Ghost\Point\Enum\OrderStatusEnum;
use Ghost\Point\Enum\PaymentMethodEnum;
use Model;

/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ghost_point_orders';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'buyer' => [
            'Ghost\Point\Models\Buyer',
            'buyer_id'
        ],
    ];

    public $belongsToMany = [
        'products' => [
            'Ghost\Point\Models\Product',
            'table' => 'ghost_point_order_product',
            'key' => 'order_id',
            'otherKey' => 'product_id',
            'timestamps' => true,
            'pivot' => [
                'price_item',
                'volume',
                'device_brand',
                'device_number',
                'device_id',
            ]
        ],
        'devices' => [
            'Ghost\Point\Models\Device',
            'table' => 'ghost_point_order_product',
            'key' => 'order_id',
            'otherKey' => 'device_id',
            'timestamps' => true,
            'pivot' => [
                'price_item',
                'volume',
                'device_brand',
                'device_number',
                'product_id',
            ]
        ]
    ];

    /**
     * @return float|int
     */
    public function getPriceAttribute()
    {
        $theme = Theme::getActiveTheme();

        // Если заправка меньше 20 литров
        // добавляем стоимость доставки
        $delivery = $this->volume < 20 ? $theme->delivery : 0 ;

        $price = array_sum($this->products->map(function($item){
           return $item->pivot->price_item * $item->pivot->volume;
        })->toArray());

        return round($delivery + $price, 2);
    }

    /**
     * @return float|int
     */
    public function getVolumeAttribute()
    {
        return array_sum($this->products->map(function($item){
            return  $item->pivot->volume;
        })->toArray());
    }

    public function getStatusLabelAttribute()
    {
        $labels = [
            OrderStatusEnum::STATUS_PAID => 'Оплачен',
            OrderStatusEnum::STATUS_PENDING => 'В обработке',
            OrderStatusEnum::STATUS_ERROR => 'Ошибка заказа',
            OrderStatusEnum::STATUS_ERROR_PAID => 'Ошибка оплаты',
            OrderStatusEnum::STATUS_CANCEL => 'Закрыт',
            OrderStatusEnum::STATUS_SUCCESS => 'Завершён',
        ];
        return $labels[$this->status] ?? $this->status;
    }

    public function getPaymentMethodLabelAttribute()
    {
        $labels = [
            PaymentMethodEnum::ONLINE => 'На сайте',
            PaymentMethodEnum::CARD => 'Картой при заправке',
        ];
        return $labels[$this->payment_method] ?? $this->payment_method;
    }

    public function getDevice(int $id)
    {
        return Device::find($id);
    }
}
