<?php


namespace Ghost\Point\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Validation;

/**
 * @author Ewgeniy Nakapyuk
 *
 */
class Settings extends Model
{
    use Validation;

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'ghost_point_settings';

    public $settingsFields = 'fields.yaml';

    public $rules = [
    ];

    /**
     * @return array
     */
    public function getPaymentTypeOptions()
    {
        return [
            'bank_card' => 'Банковской картой',
            'apple_pay' => 'Apple Pay',
            'google_pay' => 'Google Pay',
            'yandex_money' => 'Яндекс.Деньги',
            'qiwi' => 'QIWI Кошелёк',
            'webmoney' => 'Webmoney',
        ];
    }
}