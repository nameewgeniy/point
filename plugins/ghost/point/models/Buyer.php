<?php namespace Ghost\Point\Models;

use Model;

/**
 * Model
 */
class Buyer extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ghost_point_buyers';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


}
