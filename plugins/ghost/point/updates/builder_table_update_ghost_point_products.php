<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGhostPointProducts extends Migration
{
    public function up()
    {
        Schema::table('ghost_point_products', function($table)
        {
            $table->string('short')->nullable();
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ghost_point_products', function($table)
        {
            $table->dropColumn('short');
            $table->dropColumn('description');
        });
    }
}
