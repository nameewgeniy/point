<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGhostPointOrderProduct extends Migration
{
    public function up()
    {
        Schema::create('ghost_point_order_product', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->bigInteger('order_id')->nullable()->unsigned();
            $table->bigInteger('product_id')->nullable()->unsigned();
            $table->integer('price_item')->nullable()->unsigned();
            $table->integer('volume')->nullable()->unsigned();
            $table->text('device_brand')->nullable();
            $table->text('device_number')->nullable();
            $table->integer('device_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ghost_point_order_product');
    }
}
