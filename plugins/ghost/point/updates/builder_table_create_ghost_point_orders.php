<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGhostPointOrders extends Migration
{
    public function up()
    {
        Schema::create('ghost_point_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('buyer_id')->nullable();
            $table->boolean('is_active')->nullable()->default(true);
            $table->text('address')->nullable();
            $table->string('status')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ghost_point_orders');
    }
}
