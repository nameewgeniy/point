<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGhostPointCallMe extends Migration
{
    public function up()
    {
        Schema::table('ghost_point_call_me', function($table)
        {
            $table->text('message')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ghost_point_call_me', function($table)
        {
            $table->dropColumn('message');
        });
    }
}
