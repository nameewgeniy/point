<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGhostPointOrders extends Migration
{
    public function up()
    {
        Schema::table('ghost_point_orders', function($table)
        {
            $table->dateTime('delivery_date')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('ghost_point_orders', function($table)
        {
            $table->dropColumn('delivery_date');
        });
    }
}
