<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGhostPointDevices extends Migration
{
    public function up()
    {
        Schema::create('ghost_point_devices', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->text('title');
            $table->boolean('is_active')->nullable()->default(true);
            $table->integer('sort')->nullable()->default(100);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ghost_point_devices');
    }
}
