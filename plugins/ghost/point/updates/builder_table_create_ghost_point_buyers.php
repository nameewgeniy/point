<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGhostPointBuyers extends Migration
{
    public function up()
    {
        Schema::create('ghost_point_buyers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ghost_point_buyers');
    }
}
