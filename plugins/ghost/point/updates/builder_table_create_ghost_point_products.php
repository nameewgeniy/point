<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGhostPointProducts extends Migration
{
    public function up()
    {
        Schema::create('ghost_point_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->text('title')->nullable();
            $table->boolean('is_active')->nullable()->default(true);
            $table->integer('sort')->nullable()->unsigned()->default(100);
            $table->integer('price_item')->nullable()->unsigned()->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('ghost_point_products');
    }
}
