<?php namespace Ghost\Point\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGhostPointBuyers extends Migration
{
    public function up()
    {
        Schema::table('ghost_point_buyers', function($table)
        {
            $table->dropColumn('address');
        });
    }
    
    public function down()
    {
        Schema::table('ghost_point_buyers', function($table)
        {
            $table->text('address')->nullable();
        });
    }
}
