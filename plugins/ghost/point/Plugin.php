<?php namespace Ghost\Point;

use Ghost\Point\Components\ComponentCallMe;
use Ghost\Point\Components\Products\ComponentDevices;
use Ghost\Point\Components\Products\ComponentOrder;
use Ghost\Point\Components\Products\ComponentProducts;
use Ghost\Point\Console\CheckStatus;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            ComponentProducts::class => 'componentProducts',
            ComponentDevices::class => 'componentDevices',
            ComponentOrder::class => 'componentOrder',
            ComponentCallMe::class => 'componentCallMe'
        ];
    }

    public function registerSettings()
    {
        // Отоброжение настроек плагина, завязано всё на модели Settings
        return [
            'config' => [
                'label'       => 'Point',
                'icon'        => 'icon-cog',
                'description' => 'Настройки плагина',
                'class'       => 'Ghost\Point\Models\Settings',
                'permissions' => ['ghost.point.manage_plugins'],
                'order'       => 600
            ]
        ];
    }

    /**
     * Регистрация почтовых шаблонов
     * @return array
     */
    public function registerMailTemplates()
    {
        return [
            'ghost.point::mail.callme',
            'ghost.point::mail.order',
            'ghost.point::mail.buyer',
        ];
    }

    public function register()
    {
        $this->registerConsoleCommand('order.check', CheckStatus::class);
    }
}
