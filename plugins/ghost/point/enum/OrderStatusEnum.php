<?php namespace Ghost\Point\Enum;


use MyCLabs\Enum\Enum;

class OrderStatusEnum extends Enum
{
    const STATUS_PENDING = 'pending';
    const STATUS_PAID = 'paid';
    const STATUS_ERROR_PAID = 'error_paid';
    const STATUS_CANCEL = 'cancel';
    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';
}