<?php namespace Ghost\Point\Enum;


use MyCLabs\Enum\Enum;

class PaymentMethodEnum extends Enum
{
    const CARD = 'card';
    const ONLINE = 'online';
}