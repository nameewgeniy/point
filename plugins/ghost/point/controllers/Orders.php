<?php namespace Ghost\Point\Controllers;

use October\Rain\Exception\AjaxException;
use Backend\Classes\Controller;
use BackendMenu;
use Flash;
use Ghost\Point\Enum\OrderStatusEnum;
use Ghost\Point\Models\Order;

class Orders extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Ghost.Point', 'point', 'orders');
    }

    /**
     * @throws AjaxException
     */
    public function onCancel()
    {
        if(request()->ajax()) {

            if(request('checked')){

                collect(request('checked'))->map(function($item){
                    $ord = Order::find($item);
                    $ord->status = OrderStatusEnum::STATUS_SUCCESS;
                    $ord->save();

                });

                Flash::success('Заказы завершины');
                return redirect()->refresh();

            } elseif(request('Order')) {

                if($id = array_shift($this->params)){
                    $ord = Order::find($id);
                    $ord->status = OrderStatusEnum::STATUS_SUCCESS;
                    $ord->save();

                }

                Flash::success('Заказ завершён');
                return redirect()->refresh();

            } else{
                Flash::error('Заказ завершён');
            }
        } else {
            throw new AjaxException([
                'error' => 'Не удалось завершить заказ!',
            ]);
        }
    }

    public function onError()
    {
        if(request()->ajax()) {

            if(request('checked')){

                collect(request('checked'))->map(function($item){
                    $ord = Order::find($item);
                    $ord->status = OrderStatusEnum::STATUS_CANCEL;
                    $ord->save();

                });

                Flash::success('Заказы закрыты');
                return redirect()->refresh();

            } elseif(request('Order')) {

                if($id = array_shift($this->params)){
                    $ord = Order::find($id);
                    $ord->status = OrderStatusEnum::STATUS_CANCEL;
                    $ord->save();

                }

                Flash::success('Заказ закрыт');
                return redirect()->refresh();

            } else{
                Flash::error('Заказ закрыт');
            }
        } else {
            throw new AjaxException([
                'error' => 'Не удалось закрыть заказ!',
            ]);
        }
    }
}
