<?php namespace Ghost\Point\Controllers;

use Backend\Classes\Controller;
use Exception;
use Ghost\Point\Classes\Services\OrderService;
use Ghost\Point\Dto\OrderDto;
use Ghost\Point\Dto\OrderItemDto;
use Ghost\Point\Enum\OrderStatusEnum;
use Ghost\Point\Models\Order;
use Redirect;
use Request;
use Validator;

class Checkout extends Controller
{
    public $implement = [];


    public function checkout()
    {
        try {

            $validator = Validator::make(
                Request::all(),
                [
                    'products' => 'required|array',
                    'products.*.product_id' => 'required',
                    'products.*.device_id' => 'required',
                    'products.*.brand' => 'required',
                    'products.*.number' => 'required',
                    'products.*.volume' => 'required',

                    'address' => 'required',
                    'date' => 'required',
                    'name' => 'required',
                    'email' => 'required',
                    'phone' => 'required',
                    'payment' => 'required',
                ]
            );

            if ($validator->passes()) {

                $orderData = new OrderDto();
                $orderData->setAddress(request('address'));
                $orderData->setDate(request('date'));
                $orderData->setEmail(request('email'));
                $orderData->setPhone(request('phone'));
                $orderData->setName(request('name'));
                $orderData->setPaymentMethod(request('payment'));

                foreach (request('products') as $product) {

                    $orderItemData = new OrderItemDto();
                    $orderItemData->setProductId($product['product_id']);
                    $orderItemData->setDeviceId($product['device_id']);
                    $orderItemData->setDeviceBrand($product['brand']);
                    $orderItemData->setDeviceNumber($product['number']);
                    $orderItemData->setVolume($product['volume']);

                    $orderData->addItem($orderItemData);
                }

                ## Создание заказа
                $service = new OrderService();
                $order = $service->create($orderData);

                redirect(url('thanks'))->with('_order_id', $order->id);

            } else {
                return Redirect::back()->withErrors($validator);
            }

        } catch (Exception $exception) {
            return Redirect::back()->withErrors([$exception->getMessage()]);
        }
    }

    /**
     * Сюда будут приходить оповещения об оплате
     */
    public function success($id)
    {
        $order = Order::find($id);
        if($order){
            $order = $this->checkPayment($order);

            if($order->status == OrderStatusEnum::STATUS_PAID){

                return redirect(url('thanks'))->with('_order_id', $order->id);
            }
        }

        return redirect(url('error'))->with('_message', trans('ghost.point::lang.pay.error'));
    }
}
