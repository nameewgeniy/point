<?php namespace Ghost\Point\Components;

use Cms\Classes\ComponentBase;
use Ghost\Point\Models\CallMe;
use Mail;
use Validator;
use Cms\Classes\Theme;

class ComponentCallMe extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Позвонить мне',
            'description' => 'Позвонить мне'
        ];
    }

    public function onCallMe()
    {
        if (! request()->ajax()) {
            return response([
                'message' => 'Это не ajax!'
            ]);
        }

        $validator = Validator::make(
            request()->all(),
            [
                'name' => 'required|min:3',
                'phone' => 'required|min:3',
                'email' => 'required|min:3'
            ]
        );

        if ($validator->fails()) {
            return response([
                'errors' => $validator->errors()->toArray()
            ]);
        }

        $callMe = new CallMe();
        $callMe->name = request('name');
        $callMe->phone = request('phone');
        $callMe->email = request('email');

        if (request('message'))
            $callMe->message = request('message');

        $callMe->save();

        Mail::send('ghost.point::mail.callme', [
            'name' => request('name'),
            'email' => request('email'),
            'phone' => request('phone'),
            'text' => request('message'),
        ], function ($message) {
            $theme = Theme::getActiveTheme();
            $message->to($theme->email, 'Администратор');
        });

        return response([
            'message' => 'Заявка успешно отправлена!'
        ]);
    }
}