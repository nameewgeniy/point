<?php namespace Ghost\Point\Components\Products;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Exception;
use Ghost\Point\Classes\Services\OrderService;
use Ghost\Point\Dto\OrderDto;
use Ghost\Point\Dto\OrderItemDto;
use Ghost\Point\Enum\PaymentMethodEnum;
use Validator;

class ComponentOrder extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Оформление заказа',
            'description' => 'Оформление заказа'
        ];
    }

    public function onCreateOrder()
    {
        try {

            if (! request()->ajax()) {
                return response([
                    'message' => 'Это не ajax!'
                ]);
            }

            $validator = Validator::make(
                request()->all(),
                [
                    'order' => 'required',

                    'order.items' => 'required',
                    'order.items.*.product_id' => 'required',
                    'order.items.*.device_id' => 'required',
                    'order.items.*.volume' => 'required',

                    'order.buyer' => 'required',
                    'order.buyer.name' => 'required',
                    'order.buyer.phone' => 'required',
                    'order.buyer.email' => 'required',

                    'order.meta.date' => 'required',
                    'order.meta.address' => 'required',
                    'order.meta.payment_method' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response([
                    'errors' => $validator->errors()->toArray()
                ]);
            }

            $items = [];
            $order = request()->get('order');

            foreach ($order['items'] as $item) {

                $orderItem = new OrderItemDto();
                $orderItem->setProductId($item['product_id']);
                $orderItem->setDeviceId($item['device_id']);
                $orderItem->setVolume($item['volume']);
                $orderItem->setDeviceNumber($item['device_number'] ?? null);
                $orderItem->setDeviceBrand($item['device_brand'] ?? null);

                $items[] = $orderItem;
            }

            $orderItems = new OrderDto();
            $orderItems->setName($order['buyer']['name']);
            $orderItems->setPhone($order['buyer']['phone']);
            $orderItems->setEmail($order['buyer']['email']);
            $orderItems->setDate(Carbon::parse($order['meta']['date'])->toDateTimeString());
            $orderItems->setAddress($order['meta']['address']);
            $orderItems->setPaymentMethod($order['meta']['payment_method']);
            $orderItems->setItems($items);

            $service = new OrderService();

            if ($orderItems->getPaymentMethod() == PaymentMethodEnum::ONLINE)
                return $service->create($orderItems);

            $service->create($orderItems);

            return response([
                'message' => "Заказ успешно создан"
            ]);

        } catch (Exception $exception) {
            return response([
                'message' => $exception->getMessage()
            ]);
        }
    }

}