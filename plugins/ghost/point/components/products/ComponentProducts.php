<?php namespace Ghost\Point\Components\Products;

use Cms\Classes\ComponentBase;
use Ghost\Point\Models\Product;

class ComponentProducts extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Список товаров',
            'description' => 'Список товаров'
        ];
    }

    public function onRun()
    {
        $this->page['products'] = $this->getProducts();
    }

    private function getProducts()
    {
        return Product::orderBy('sort', 'desc')
            ->get();
    }
}