<?php namespace Ghost\Point\Components\Products;

use Cms\Classes\ComponentBase;
use Ghost\Point\Models\Device;
use Ghost\Point\Models\Product;

class ComponentDevices extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Список техники',
            'description' => 'Список техники'
        ];
    }

    public function onRun()
    {
        $this->page['devices'] = $this->getDevices();
    }

    private function getDevices()
    {
        return Device::orderBy('sort', 'desc')
            ->get();
    }
}