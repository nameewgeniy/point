<?php namespace Ghost\Point\Classes\Services;


use Cms\Classes\Theme;
use DomainException;
use Event;
use Exception;
use Ghost\Point\Dto\OrderItemDto;
use Ghost\Point\Enum\OrderStatusEnum;
use Ghost\Point\Enum\PaymentMethodEnum;
use Ghost\Point\Interfaces\OrderServiceInterface;
use Ghost\Point\Models\Buyer;
use Ghost\Point\Models\Order;
use Ghost\Point\Models\Product;
use Ghost\Point\Dto\OrderDto;
use Illuminate\Support\Facades\DB;
use Mail;
use Throwable;

class OrderService implements OrderServiceInterface
{

    /**
     * @param OrderDto $orderDto
     * @return mixed
     * @throws Throwable
     */
    public function create(OrderDto $orderDto)
    {
        return DB::transaction(function () use ($orderDto) {
            try {

                $theme = Theme::getActiveTheme();

                $buyer = Buyer::wherePhone($orderDto->getPhone())->first() ?: new Buyer();
                $buyer->name = $orderDto->getName();
                $buyer->phone = $orderDto->getPhone();
                $buyer->email = $orderDto->getEmail();
                $buyer->save();

                $order = new Order();
                $order->is_active = true;
                $order->delivery_date = $orderDto->getDate();
                $order->address = $orderDto->getAddress();
                $order->payment_method = $orderDto->getPaymentMethod();
                $order->status = OrderStatusEnum::STATUS_PENDING;
                $order->buyer_id = $buyer->id;
                $order->save();

                collect($orderDto->getItems())->map(function ($item) use ($order){

                    /** @var $item OrderItemDto */
                    $product = Product::findOrFail($item->getProductId());

                    $order->products()->attach($product->id, [
                        'price_item' => $product->price_item,
                        'volume' => $item->getVolume(),
                        'device_brand' => $item->getDeviceBrand(),
                        'device_number' => $item->getDeviceNumber(),
                        'device_id' => $item->getDeviceId(),
                    ]);
                });

                Mail::send('ghost.point::mail.order', [
                    'order' => $order
                ], function ($message) use ($theme) {

                    $message->to($theme->email, 'Администратор');
                });

                Mail::send('ghost.point::mail.buyer', [
                    'order' => $order,
                    'phone' => $theme->c_phone
                ], function ($message) use ($buyer) {
                    $message->to($buyer->email, 'Администратор');
                });

                # Если способ оплаты на сайте, отпарвляем на оплату
                if ($order->payment_method == PaymentMethodEnum::ONLINE) {

                    $payment = new PaymentService();
                    $result = $payment->setOrder($order)
                        ->pay();

                    if (! $result->id)
                        throw new DomainException('Не удалось создать заказ');

                    $order->payment_id = $result->id;
                    $order->save();

                    // Redirect
                    return $payment->run();
                }

            } catch (Exception $exception) {

                Event::fire('ghost.point.order.create.error', [$exception]);
                throw new DomainException($exception->getMessage());
            }
        });
    }

    /**
     *
     */
    public function checkStatus()
    {
        $payment = new PaymentService();

        $orders = Order::whereStatus(OrderStatusEnum::STATUS_PENDING)
            ->get();

        if(! $orders->isEmpty()){
            $orders->map(function($item) use ($payment) {
                $payment->checkPayment($item);
            });
        }
    }
}