<?php


namespace Ghost\Point\Classes\Services;


use DomainException;
use Ghost\Point\Enum\OrderStatusEnum;
use Ghost\Point\Models\Order;
use Ghost\Point\Models\Settings;
use Redirect;
use YandexCheckout\Client;
use YandexCheckout\Common\Exceptions\ApiException;
use YandexCheckout\Common\Exceptions\BadApiRequestException;
use YandexCheckout\Common\Exceptions\ExtensionNotFoundException;
use YandexCheckout\Common\Exceptions\ForbiddenException;
use YandexCheckout\Common\Exceptions\InternalServerError;
use YandexCheckout\Common\Exceptions\NotFoundException;
use YandexCheckout\Common\Exceptions\ResponseProcessingException;
use YandexCheckout\Common\Exceptions\TooManyRequestsException;
use YandexCheckout\Common\Exceptions\UnauthorizedException;
use YandexCheckout\Request\Payments\CreatePaymentResponse;

class PaymentService
{

    private $shopId;
    private $token;
    private $mode;
    private $redirectUrl;
    private $client;
    private $order;
    private $payment;
    private $currency = 'RUB';

    /**
     * PaymentService constructor.
     */
    public function __construct()
    {
        $this->shopId      = Settings::get('shop_id', '735311');
        $this->token       = Settings::get('token', 'fPOaMKOKDv9rU0qqwVeWGxZ6lYk7VpqP9J6oi-XeZOg');
        $this->mode        = Settings::get('mode', false);
        $this->redirectUrl = Settings::get('redirect_url', '/success');

        $this->client = new Client();
        $this->client->setAuth($this->shopId, $this->getToken());
    }

    /**
     * @return mixed
     */
    private function getToken()
    {
        return $this->mode
            ? $this->token
            : 'test_' . $this->token;
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return CreatePaymentResponse|null
     * @throws ApiException
     * @throws BadApiRequestException
     * @throws ForbiddenException
     * @throws InternalServerError
     * @throws NotFoundException
     * @throws ResponseProcessingException
     * @throws TooManyRequestsException
     * @throws UnauthorizedException
     */
    public function pay()
    {
        return $this->payment = $this->client->createPayment([
            'description' => 'Заказ номер: ' . $this->order->id,
            'amount' => [
                'value' => $this->order->price,
                'currency' => $this->currency,
            ],
            'metadata' => [
                'id' => $this->order->id
            ],
            'payment_method_data' => [
                'type' => 'bank_card'
            ],
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => url($this->redirectUrl),
            ],
            'capture' => true
        ], uniqid('', true));
    }

    /**
     * @return mixed
     */
    public function run()
    {
        return Redirect::to($this->getConfirmationUrl($this->payment));
    }

    /**
     * @param $payment
     * @return null
     */
    protected function getConfirmationUrl($payment)
    {
        $confirmation = $payment->getConfirmation();
        if($confirmation && $confirmation->type == 'redirect'){
            return $confirmation->confirmationUrl;
        } else { return null; }
    }

    /**
     * @param Order $order
     * @return Order
     * @throws ApiException
     * @throws BadApiRequestException
     * @throws ForbiddenException
     * @throws InternalServerError
     * @throws NotFoundException
     * @throws ResponseProcessingException
     * @throws TooManyRequestsException
     * @throws UnauthorizedException
     * @throws ExtensionNotFoundException
     */
    public function checkPayment(Order $order)
    {
        if(! empty($order->payment_id)){

            if (! $pay = $this->client->getPaymentInfo($order->payment_id))
                throw new DomainException('Не получилось получить информацию о платеже');

            if ( $pay->getStatus() == 'succeeded') {
                $order->status = OrderStatusEnum::STATUS_PAID;

            } elseif ($pay->getStatus() == 'pending') {
                $order->status = OrderStatusEnum::STATUS_PENDING;

            } else {
                $order->status = OrderStatusEnum::STATUS_ERROR_PAID;

            }

            $order->save();
        }

        return $order;
    }

}