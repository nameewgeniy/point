<?php

namespace Ghost\Point\Tests\Unit\Order;

use Carbon\Carbon;
use Ghost\Point\Classes\Services\OrderService;
use Ghost\Point\Classes\Services\PaymentService;
use Ghost\Point\Dto\OrderDto;
use Ghost\Point\Dto\OrderItemDto;
use Ghost\Point\Enum\OrderStatusEnum;
use Ghost\Point\Enum\PaymentMethodEnum;
use Ghost\Point\Models\Device;
use Ghost\Point\Models\Product;
use Ghost\Point\Tests\PluginTestCase;


class OrderTest extends PluginTestCase
{
    public function testCreateOrder()
    {
        $this->markTestSkipped();
        $product = new Product();
        $product->title = 'Бензин 95';
        $product->is_active = true;
        $product->sort = 100;
        $product->price_item = 100;
        $product->save();

        $device = new Device();
        $device->title = 'Машина';
        $device->is_active = true;
        $device->sort = 100;
        $device->save();

        /** @var OrderService $orderService */
        $orderService = app()->make(OrderService::class);

        $orderItemData = new OrderItemDto();
        $orderItemData->setProductId($product->id);
        $orderItemData->setDeviceId($device->id);
        $orderItemData->setDeviceBrand('Toyota');
        $orderItemData->setDeviceNumber('x104ty');
        $orderItemData->setVolume(2);

        $orderItemDataSecond = new OrderItemDto();
        $orderItemDataSecond->setProductId($product->id);
        $orderItemDataSecond->setDeviceId($device->id);
        $orderItemDataSecond->setDeviceBrand('Kia Ceed');
        $orderItemDataSecond->setDeviceNumber('x104ty');
        $orderItemDataSecond->setVolume(5);

        $orderData = new OrderDto();
        $orderData->setAddress('Новый адрес');
        $orderData->setDate($date = Carbon::now()->format('d-m-Y H:i'));
        $orderData->setEmail('nameewgeniy@ya.ru');
        $orderData->setPhone('89385122510');
        $orderData->setName('Евгений Петрович');
        $orderData->setPaymentMethod(PaymentMethodEnum::ONLINE);
        $orderData->setItems([
            $orderItemData,
            $orderItemDataSecond,
        ]);

        $order = $orderService->create($orderData);

        $this->assertEquals(1, $order->id);
        $this->assertEquals(OrderStatusEnum::STATUS_PENDING, $order->status);
        $this->assertCount(2, $order->products);
        $this->assertEquals(true, $order->is_active);
        $this->assertNotEmpty($order->payment_id);
        $this->assertEquals('Новый адрес', $order->address);
        $this->assertEquals(PaymentMethodEnum::ONLINE, $order->payment_method);
        $this->assertEquals($date, $order->delivery_date);

        $this->assertEquals('Евгений Петрович', $order->buyer->name);
        $this->assertEquals('89385122510', $order->buyer->phone);
        $this->assertEquals('nameewgeniy@ya.ru', $order->buyer->email);

        $order->products->map(function ($item) use ($product, $device){

            $this->assertEquals($product->id, $item->id);
            $this->assertEquals($product->price_item, $item->pivot->price_item);
            $this->assertEquals($device->id, $item->pivot->device_id);
            $this->assertEquals('x104ty', $item->pivot->device_number);

            if ($item->pivot->id == 1) {
                $this->assertEquals(2, $item->pivot->volume);
                $this->assertEquals('Toyota', $item->pivot->device_brand);
            }

            if ($item->pivot->id == 2) {
                $this->assertEquals(5, $item->pivot->volume);
                $this->assertEquals('Kia Ceed', $item->pivot->device_brand);
            }
        });


        return $stack['order'] = $order;
    }

    public function testCheckStatus()
    {
        $st = new OrderService();
        $st->checkStatus();
    }

}