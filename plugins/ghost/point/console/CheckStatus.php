<?php namespace Ghost\Point\Console;


use Ghost\Point\Classes\Services\OrderService;
use Illuminate\Console\Command;

class CheckStatus extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'order.check';

    /**
     * @var string The console command description.
     */
    protected $description = 'Order check';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->output->writeln('Запускаю пересчёт статусов');
        $service = new OrderService();
        $service->checkStatus();
    }

}