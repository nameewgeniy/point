CONTAINER = @docker exec -it points_php_1

bash:
	$(CONTAINER) bash

composer:
	$(CONTAINER) composer $(filter-out $@,$(MAKECMDGOALS))

artisan:
	$(CONTAINER) php artisan $(filter-out $@,$(MAKECMDGOALS))

test:
	$(CONTAINER) vendor/bin/phpunit